#ifndef MIRRORFINDERCLASS_H
#define MIRRORFINDERCLASS_H

#include <QObject>
#include <QSysInfo>
#include <QDir>
#include <QSettings>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include<QRegularExpressionMatch>
#include <QList>
#include <QTime>
#include <QMap>

class MirrorFinderClass : public QObject
{
    Q_OBJECT
public:
    explicit MirrorFinderClass(QObject *parent = nullptr);
    void startPrograme();

signals:

private:

    //variables
    QString m_currentRelease; //save info version use fedora
    QString m_curretArchtitecture; //save info cpu architecture
    QSysInfo m_findInformation; // find information version release fedora and architecture

    QString m_metaLinkFedora; //adress metalink Fedora
    QString m_metaLinkFedoraUpdate; // adress metalink Fedora Update

    QNetworkAccessManager * m_manager;
    QNetworkRequest m_request;

    QList<QString> m_queueFedoraRepo; // save links

    QTime m_time;
    QMap<int , QString> m_calculateTimeFedoraRepository; //save calculate time and links Fedora repsitory

    QMap<int , QString> m_calculateTimeFedoraUpdatesRepository; //save calculate time and links Fedora-updates repsitory

    QUrl m_urlTakeFirstItemFedoraRepository; // save linke take first item from qeue

    QUrl m_urlTakeFirstItemFedoraUpdatesRepository;

    QString m_dynamicLinkFedora; // create dynamic link fedora

    QString m_chekLinkFedoraRepoFuture; // save links for future check another links

    // fedora-update-repo
    QList<QString> m_queueFedoraUpdateRepo;

    QString m_dynamicLinkFedoraUpdatesRepo;

    //fucntion
    void findInfoSystem(); //find info version fedora and cpu architecture

    void createPathForIniFilesDownloadLinksMetaLinksAndDownloadLinksExtractedMetalinks(); //create path /etc/yum.repos.d/IniFiles

    void createLinkDownloadMetalinkFedora(); //create link metaLink from ini file  use help valuse on ini file for download just Download metalink Fedora

    void downloadMetaLinks(); // after create meta links now this function start download thats metalink

    void downloadFinishedMetalinkFedora(QNetworkReply *downloadFinishedMetalinkFedora); //finish save and run regex for take out links on file

    void readLinksFirstItemOfListFedoraRepo(); // download first links fedora repository

    void downloadItemsFedoraRepository(QUrl takeFirst); // download items on queue fedora repostiory

    void downloadItemsFedoraRepositoryFinished(QNetworkReply *downloadFinishedTakeFirstFedoraRepository); // finished download on queu fedora repostiory

    void createDynamicLinkForDownloadPackageFromFedoraRepository(QString takeLink); //download links package from links

    void downloadPageFedoraMainForCompletionLink(); // download page for find version package gcc

    void finishdownloadPageFedoraMainForCompletionLink(QNetworkReply *finishDownloadPageFedoraRepo); //finish download links page and run regex for find version

    void writToRepoFedora(); //open ini file repository fedora and write to it

    void fixSharp(const QString &addressRepository); // open repository and change all %23 to # and comment all metalinks

    //start Fedora-Update.repo
    void createLinkdDownloadMetalinkFedoraUpdate(); // create metalink for download  metalink  repository fedora-update

    void downloadMetalinkFedoraUpdate(); // start download metalink repository fedoar-update

    void finishDownloadMetalinkFedoraUpdate(QNetworkReply *donwloadFinishedMetalinkReadData); // finished download metalink and run regex

    void writeToRepoFedoraUpdatesRepo(bool firstItem); // write to fedora-updates.repo

    void readLinksFirstItemOfListFedoraUpdatesRepo();

    void createDynamicLinkForDownloadPackageFromFedoraUpdatesRepository(QString takeLink); //download links package from links

    void downloadPageFedoraUpdatesForCompletionLink(); // download page for find version package gcc

    void finishDownloadPageFedoraUpdatesForCompletionLink(QNetworkReply *finishDownloadPageFedoraUPdate);

    void downloadItemsFedoraUpdatesRepository(QUrl takeFirst);

    void downloadItemsFedoraUpdateRepositoryFinished(QNetworkReply *downloadFinishedTakeFirstFedoraUpdatesRepository);

    //regex fedora
    QRegularExpression m_regexMetalLinkFedoraRepo{"http(:?s|)://.*/os"}; // find metalink fedora
    QRegularExpression m_regexFindPackageFedoraRepoMainAndUpdate{"gcc-\\d.*.rpm"}; //find pakcage from
    QRegularExpression m_regexFindLinksForCheckFuture{"http(:?s|)://.*/(fedora:?|Fedora)/"}; //find links for check future

    //regex fedora-update
    QRegularExpression m_regexMetalinkFedoraUpdateRepo{"http(:?s|)://.*repomd.xml"};
};
#endif // MIRRORFINDERCLASS_H
