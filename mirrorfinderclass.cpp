﻿#include "mirrorfinderclass.h"
#define ROOT_ETC_YUM_PATH "/etc/yum.repos.d/"
#define BYTE_DOWNLOAD_ 500000

MirrorFinderClass::MirrorFinderClass(QObject *parent) : QObject(parent)
{
}

void MirrorFinderClass::startPrograme()
{
    qDebug() << __FUNCTION__;

    findInfoSystem();
    createPathForIniFilesDownloadLinksMetaLinksAndDownloadLinksExtractedMetalinks();
    createLinkDownloadMetalinkFedora();
}

void MirrorFinderClass::findInfoSystem()//find info version fedora and cpu architecture
{
    m_currentRelease = m_findInformation.productVersion(); //save current version fedora
    m_curretArchtitecture = m_findInformation.currentCpuArchitecture(); //save cpu architectureM
}

void MirrorFinderClass::createPathForIniFilesDownloadLinksMetaLinksAndDownloadLinksExtractedMetalinks() //create directory IniFiles and file ini  alghoritmMetalinksRepository.ini file into address /etc/yum.repo.d/IniFiles/
{
    qDebug() << __FUNCTION__;


    QDir dir(ROOT_ETC_YUM_PATH "IniFiles");
    if(!dir.exists())
    {
        dir.mkpath(ROOT_ETC_YUM_PATH "IniFiles");
    }

    QSettings settings(ROOT_ETC_YUM_PATH "IniFiles/alghoritmMetalinksMetalinkRepository.ini" , QSettings::IniFormat); // create ini file alghoritmMetalinksMetalinkRepository

    // values metalinks
    settings.beginGroup("metaLinkFedora");
    settings.setValue("alghorithmMetalinkFedoraMain" , "https://mirrors.fedoraproject.org/metalink?repo=fedora-$releasever&arch=$basearch&country=global"); // set values
    settings.endGroup();

    settings.beginGroup("metalLinkFedoraUpdate");
    settings.setValue("alghorithmMetalinkFedoraUpdateMain" ,"https://mirrors.fedoraproject.org/metalink?repo=updates-released-f$releasever&arch=$basearch&country=global");
    settings.endGroup();


    //create ini file alghoritmDynamicLink.ini for create dynamic link fedora repository
    QSettings settingsDynamicLink(ROOT_ETC_YUM_PATH "IniFiles/alghoritmDynamicLink.ini" , QSettings::IniFormat);

    // values create Dynamic Link
    settingsDynamicLink.beginGroup("dynamicLinkFedoraMain");
    settingsDynamicLink.setValue("alghorithmMetalinkFedoraMain" , "$link/Packages/g/");
    settingsDynamicLink.endGroup();
}

void MirrorFinderClass::createLinkDownloadMetalinkFedora() // create link metalink repository
{
    qDebug() << __FUNCTION__;

    QSettings settings(ROOT_ETC_YUM_PATH "IniFiles/alghoritmMetalinksMetalinkRepository.ini" , QSettings::IniFormat); //open ini file

    settings.beginGroup("metaLinkFedora");
    m_metaLinkFedora =settings.value("alghorithmMetalinkFedoraMain").toString(); // save values  into ini file to variable m_metalinkFedora
    settings.endGroup();

    m_metaLinkFedora.replace("$releasever" , m_currentRelease); // replace release version fedora
    m_metaLinkFedora.replace("$basearch" , m_curretArchtitecture); // replace acrfhitecture cpu
    downloadMetaLinks(); // call  functiion downloadMetaLinks for start download metalink
}

void MirrorFinderClass::downloadMetaLinks() //download metalinks repositoryfedora
{
    qDebug() << __FUNCTION__; //show name function

    //start download metalink from url m_metaLinkFedora
    m_manager = new QNetworkAccessManager(this);
    connect(m_manager , &QNetworkAccessManager::finished , this , &MirrorFinderClass::downloadFinishedMetalinkFedora); //after finish download call function  downloadFinishedMetalinkFedora
    QUrl url = m_metaLinkFedora;
    m_request.setUrl(url);
    m_manager->get(m_request);

}

void MirrorFinderClass::downloadFinishedMetalinkFedora(QNetworkReply *downloadFinishedMetalinkFedora)  //finish download metalink fedora
{
    qDebug() << __FUNCTION__;

    QByteArray _dataMetalinkFinished = downloadFinishedMetalinkFedora->readAll(); // save all values from url m_metaLinkFedora to varialbe _dataMetalinkFinished
    QString _allDataMetalink = _dataMetalinkFinished; // save values _dataMetalinkFinished to variable   _allDataMetalink

    //run regex and Extraction links from metalink fedora
    QRegularExpressionMatchIterator i = m_regexMetalLinkFedoraRepo.globalMatch(_allDataMetalink);
    QStringList _linksMetalinkFedora;  //save links
    while (i.hasNext())
    {
        QRegularExpressionMatch match = i.next();
        QString word = match.captured();
        _linksMetalinkFedora << word;
    }
    m_queueFedoraRepo = _linksMetalinkFedora; // save links finded use regex to variable  queue m_queueFedoraRepo
    readLinksFirstItemOfListFedoraRepo(); //call this function for start download links in queu

}

void MirrorFinderClass::readLinksFirstItemOfListFedoraRepo() //recursive function
{
    qDebug () << __FUNCTION__ << m_queueFedoraRepo.count() << "Items remained"; // show count items links
    if(!m_queueFedoraRepo.isEmpty())
    {
        QString _takelink = m_queueFedoraRepo.takeFirst(); // recursive function and after recursive take first itme from list and save to variable _takelink
        createDynamicLinkForDownloadPackageFromFedoraRepository(_takelink); // send first itme for function start download
    }

    else
    {
        writToRepoFedora();
    }

}

void MirrorFinderClass::createDynamicLinkForDownloadPackageFromFedoraRepository(QString takeLink) //  create dynamic link from repository fedroa
{
    qDebug() << __FUNCTION__;

    QSettings settings(ROOT_ETC_YUM_PATH "IniFiles/alghoritmDynamicLink.ini" , QSettings::IniFormat); //open ini file

    settings.beginGroup("dynamicLinkFedoraMain");
    m_dynamicLinkFedora =settings.value("alghorithmMetalinkFedoraMain").toString(); // save values  into ini file to variable m_metalinkFedora
    settings.endGroup();

    m_dynamicLinkFedora.replace("$link" , takeLink); // replace release version fedora
    downloadPageFedoraMainForCompletionLink();

}

void MirrorFinderClass::downloadPageFedoraMainForCompletionLink() // download page package gcc
{
    qDebug() << __FUNCTION__;

    m_manager = new QNetworkAccessManager(this);
    m_request.setUrl(m_dynamicLinkFedora);
    m_manager->get(m_request);
    connect(m_manager , &QNetworkAccessManager::finished , this , &MirrorFinderClass::finishdownloadPageFedoraMainForCompletionLink);
}

void MirrorFinderClass::finishdownloadPageFedoraMainForCompletionLink(QNetworkReply *finishDownloadPageFedoraRepo) // finish donwload page and find version gcc use regex
{
    qDebug() << __FUNCTION__;

    if(finishDownloadPageFedoraRepo->error())
    {
        qDebug () << "Error";
        qDebug () << finishDownloadPageFedoraRepo->errorString();
        readLinksFirstItemOfListFedoraRepo();
    }
    else
    {
        //find version
        QByteArray _dataReadAllPageDownload = finishDownloadPageFedoraRepo->readAll();
        QString _datastring = _dataReadAllPageDownload;
        QRegularExpressionMatchIterator i = m_regexFindPackageFedoraRepoMainAndUpdate.globalMatch(_datastring);
        QStringList _findVersionPackageGcc; ; //save links
        //start find version gcc
        while (i.hasNext())
        {
            QRegularExpressionMatch match = i.next();
            QString word = match.captured();
            _findVersionPackageGcc << word;
        }

        QString _takeOutGccVersionFromList  = _findVersionPackageGcc.join("");
        int pos = _takeOutGccVersionFromList.indexOf(">");
        QString cutThis = _takeOutGccVersionFromList.right(pos); //remove additional after run regex for find version package gcc
        cutThis.remove(">"); // remove > into string Version
        m_dynamicLinkFedora += cutThis;

        downloadItemsFedoraRepository(m_dynamicLinkFedora);
    }
}

void MirrorFinderClass::downloadItemsFedoraRepository(QUrl takeFirst) //download package and find amout time download
{
    qDebug() << __FUNCTION__;
    m_time.start(); // start time for find amount of time download package from link
    m_urlTakeFirstItemFedoraRepository = takeFirst.toString(); // save value takefirts to variable m_urlTakeFirstItemFedoraRepository
    m_manager = new QNetworkAccessManager(this);
    m_request.setRawHeader("Range" , "bytes=" + QByteArray::number(0) + "-" + QByteArray::number(BYTE_DOWNLOAD_)); // set download 500 bytes from link
    m_request.setUrl(takeFirst);
    m_manager->get(m_request);
    connect(m_manager , &QNetworkAccessManager::finished , this , &MirrorFinderClass::downloadItemsFedoraRepositoryFinished);

}

void MirrorFinderClass::downloadItemsFedoraRepositoryFinished(QNetworkReply *downloadFinishedTakeFirstFedoraRepository)
{
    qDebug() << __FUNCTION__;

    if(downloadFinishedTakeFirstFedoraRepository->error())
    {
        qDebug () << "Error";
        qDebug () << downloadFinishedTakeFirstFedoraRepository->errorString();
        readLinksFirstItemOfListFedoraRepo();
    }
    else
    {
        QString finishLinksForItems = m_urlTakeFirstItemFedoraRepository.toString();
        m_calculateTimeFedoraRepository.insert(m_time.elapsed() ,finishLinksForItems ); // save  links and time is key and value is links

        readLinksFirstItemOfListFedoraRepo();
    }
}

void MirrorFinderClass::writToRepoFedora() //find repository , open repoitory , write to repository
{
    qDebug() << __FUNCTION__;
    qDebug()<< "your best link for Repo Fedora.repo is " << m_calculateTimeFedoraRepository.first(); //show link good for repository fedora


    QDir directory(ROOT_ETC_YUM_PATH);
    QStringList repofiles = directory.entryList(QStringList() << "*.repo" , QDir::Files);
    QString _addressRepoFedora;

    for(int i = 0 ; i < repofiles.size() ; i++) // start search into list repository for find  fedora.repo
    {

        if(repofiles.at(i) == "fedora.repo")
        {
            _addressRepoFedora = directory.path();
            _addressRepoFedora.append("/");
            _addressRepoFedora.append(repofiles.at(i));

            QString _writFirst = m_calculateTimeFedoraRepository.first();

            int pos = _writFirst.lastIndexOf(QString("Packages/"));
            QString _finalLink =_writFirst.left(pos);
            QSettings _settings(_addressRepoFedora , QSettings::NativeFormat);

            _settings.beginGroup("fedora");
            _settings.setValue("baseurl" ,_finalLink );
            _settings.endGroup();

            //start regex for Extraction link untile the fedora (Fedora)
            QRegularExpressionMatchIterator i = m_regexFindLinksForCheckFuture.globalMatch(_finalLink);
            QString _saveLink;
            while (i.hasNext())
            {
                QRegularExpressionMatch match = i.next();
                QString word = match.captured();
                m_chekLinkFedoraRepoFuture = word;
            }
        }
    }
    fixSharp(_addressRepoFedora);


    qDebug() << "start Fedora-Update.repo";
    createLinkdDownloadMetalinkFedoraUpdate();
}

void MirrorFinderClass::fixSharp(const QString &addressRepository) //open file ad fix all %23 and comment metalinks
{
    qDebug() << __FUNCTION__;
    QFile file(addressRepository);
    file.open(QFile::ReadOnly);

    QByteArray data = file.readAll();
    data.replace("%23baseurl=", "#baseurl=");
    data.replace("metalink=" , "#metalink=");

    file.close();

    file.open(QFile::WriteOnly);
    file.write(data);
    file.close();
}

// start Fedora-Update.repo
void MirrorFinderClass::createLinkdDownloadMetalinkFedoraUpdate()
{
    qDebug() << __FUNCTION__;

    QSettings settings(ROOT_ETC_YUM_PATH "IniFiles/alghoritmMetalinksMetalinkRepository.ini" , QSettings::IniFormat); //open ini file

    settings.beginGroup("metalLinkFedoraUpdate");
    m_metaLinkFedoraUpdate =settings.value("alghorithmMetalinkFedoraUpdateMain").toString(); // save values  into ini file to variable m_metalinkFedora
    settings.endGroup();

    m_metaLinkFedoraUpdate.replace("$releasever" , m_currentRelease); // replace release version fedora
    m_metaLinkFedoraUpdate.replace("$basearch" , m_curretArchtitecture); // replace acrfhitecture cpu
    downloadMetalinkFedoraUpdate(); // call  functiion downloadMetaLinks for start download metalink
}

void MirrorFinderClass::downloadMetalinkFedoraUpdate()
{
    qDebug() << __FUNCTION__;

    m_manager = new QNetworkAccessManager(this);
    connect(m_manager , &QNetworkAccessManager::finished , this , &MirrorFinderClass::finishDownloadMetalinkFedoraUpdate);
    QUrl url = m_metaLinkFedoraUpdate;
    m_request.setUrl(url);
    m_manager->get(m_request);
}
void MirrorFinderClass::finishDownloadMetalinkFedoraUpdate(QNetworkReply *donwloadFinishedMetalinkReadData)
{
    qDebug() << __FUNCTION__;

    QByteArray _dataMetalinks = donwloadFinishedMetalinkReadData->readAll(); // save all values from url m_metaLinkFedora to varialbe _dataMetalinkFinished
    QString _dataMetalinksFinished = _dataMetalinks;

    //start regex for find and cut links untile fedora(Fedora)
    QRegularExpressionMatchIterator i = m_regexFindLinksForCheckFuture.globalMatch(_dataMetalinksFinished);
    QStringList _linksUntilFedora; // write links to  this variable
    while (i.hasNext())
    {
        QRegularExpressionMatch math = i.next();
        QString word = math.captured();
        _linksUntilFedora << word;
    }

    bool _checkStautsFinishProgrameOrNot; //check repository is duplicate or not

    // start for check have thats links into metalink or not
    for(int i{0} ; i < _linksUntilFedora.size() ; i++)
    {
        if(_linksUntilFedora.at(i) == m_chekLinkFedoraRepoFuture) // if have start to write repostiory fedora-updates.repo
        {
            _checkStautsFinishProgrameOrNot = true;
        }
    }

    if(_checkStautsFinishProgrameOrNot == true)
    {
        writeToRepoFedoraUpdatesRepo(true); // send true because link is last link and just need write to fedora-updates.repo
    }

    else
    {
        // if thats link not into metalink fedora.repo start for find best links
        QRegularExpressionMatchIterator j= m_regexMetalinkFedoraUpdateRepo.globalMatch(_dataMetalinksFinished);
        QStringList _linksMetalink;

        while (j.hasNext())
        {
            QRegularExpressionMatch match = j.next();
            QString word = match.captured();
            _linksMetalink << word;
        }
        m_queueFedoraUpdateRepo = _linksMetalink;

        readLinksFirstItemOfListFedoraUpdatesRepo();
    }

}
void MirrorFinderClass::writeToRepoFedoraUpdatesRepo(bool firstItem)
{
    qDebug() << __FUNCTION__;
    QDir directory(ROOT_ETC_YUM_PATH);
    QStringList repofiles = directory.entryList(QStringList() << "*.repo" , QDir::Files);
    QString _addressRepoFedora;

    if(firstItem == true)
    {
        qDebug()<< "your best link for Repo Fedora-update is " << m_calculateTimeFedoraRepository.first(); //show link good for repository fedora
        for(int i = 0 ; i < repofiles.size() ; i++) // start search into list repository for find  fedora.repo
        {

            if(repofiles.at(i) == "fedora-updates.repo")
            {
                _addressRepoFedora = directory.path();
                _addressRepoFedora.append("/");
                _addressRepoFedora.append(repofiles.at(i));

                QString _writFirst = m_calculateTimeFedoraRepository.first();

                int pos = _writFirst.lastIndexOf(QString("os/"));
                QString _finalLink =_writFirst.left(pos);

                _finalLink.replace("releases" , "updates" );

                QSettings _settings(_addressRepoFedora , QSettings::NativeFormat);

                _settings.beginGroup("updates");
                _settings.setValue("baseurl" ,_finalLink );
                _settings.endGroup();
            }
        }
    }

    else
    {
        qDebug()<< "your best link for Repo Fedora-update is " << m_calculateTimeFedoraUpdatesRepository.first(); //show link good for repository fedora

        for(int i = 0 ; i < repofiles.size() ; i++) // start search into list repository for find  fedora.repo
        {

            if(repofiles.at(i) == "fedora-updates.repo")
            {
                _addressRepoFedora = directory.path();
                _addressRepoFedora.append("/");
                _addressRepoFedora.append(repofiles.at(i));

                QString _writFirst = m_calculateTimeFedoraUpdatesRepository.first();

                int pos = _writFirst.lastIndexOf(QString("Everything/"));
                QString _finalLink =_writFirst.left(pos);
                _finalLink += "Everything/$currentArch/";
                _finalLink.replace("$currentArch" , m_curretArchtitecture);

                QSettings _settings(_finalLink , QSettings::NativeFormat);

                _settings.beginGroup("updates");
                _settings.setValue("baseurl" ,_finalLink );
                _settings.endGroup();

                fixSharp(_addressRepoFedora);
            }
        }

    }

    fixSharp(_addressRepoFedora);
    qDebug() << "Finish Program";

}

void MirrorFinderClass::readLinksFirstItemOfListFedoraUpdatesRepo()
{
    qDebug () << __FUNCTION__ << m_queueFedoraUpdateRepo.count() << "Items remained"; // show count items links
    if(!m_queueFedoraUpdateRepo.isEmpty())
    {
        QString _takelink = m_queueFedoraUpdateRepo.takeFirst(); // recursive function and after recursive take first itme from list and save to variable _takelink
        createDynamicLinkForDownloadPackageFromFedoraUpdatesRepository(_takelink); // send first itme for function start download
    }

    else
    {
        writeToRepoFedoraUpdatesRepo(false); // send false because finde new link
    }

}

void MirrorFinderClass::createDynamicLinkForDownloadPackageFromFedoraUpdatesRepository(QString takeLink) //download links package from links
{
    qDebug() << __FUNCTION__;

    int pos = takeLink.lastIndexOf(QString("repodata/"));
    QString _finalLink =takeLink.left(pos);
    _finalLink +="Packages/g/";
    takeLink = _finalLink;

    m_dynamicLinkFedoraUpdatesRepo = takeLink;
    downloadPageFedoraUpdatesForCompletionLink(); // download page for find version package gcc
}
void MirrorFinderClass::downloadPageFedoraUpdatesForCompletionLink()
{
    qDebug() << __FUNCTION__;

    m_manager = new QNetworkAccessManager(this);
    m_request.setUrl(m_dynamicLinkFedoraUpdatesRepo);
    m_manager->get(m_request);
    connect(m_manager , &QNetworkAccessManager::finished , this , &MirrorFinderClass::finishDownloadPageFedoraUpdatesForCompletionLink);
}

void MirrorFinderClass::finishDownloadPageFedoraUpdatesForCompletionLink(QNetworkReply *finishDownloadPageFedoraUPdate)
{
    qDebug() << __FUNCTION__;

    if(finishDownloadPageFedoraUPdate->error())
    {
        qDebug () << "Error";
        qDebug () << finishDownloadPageFedoraUPdate->errorString();
        readLinksFirstItemOfListFedoraUpdatesRepo();
    }
    else
    {
        //find version
        QByteArray _dataReadAllPageDownload = finishDownloadPageFedoraUPdate->readAll();
        QString _datastring = _dataReadAllPageDownload;
        QRegularExpressionMatchIterator i = m_regexFindPackageFedoraRepoMainAndUpdate.globalMatch(_datastring);
        QStringList _findVersionPackageGcc; //save links
        //start find version gcc
        while (i.hasNext())
        {
            QRegularExpressionMatch match = i.next();
            QString word = match.captured();
            _findVersionPackageGcc << word;
        }

        QString _takeOutGccVersionFromList = _findVersionPackageGcc.join("");
        int pos = _takeOutGccVersionFromList.lastIndexOf(QString("\""));
        QString cutThis = _takeOutGccVersionFromList.right(pos); //remove additional after run regex for find version package gcc
        m_dynamicLinkFedoraUpdatesRepo += cutThis;
        downloadItemsFedoraUpdatesRepository(m_dynamicLinkFedoraUpdatesRepo);
    }
}
void MirrorFinderClass::downloadItemsFedoraUpdatesRepository(QUrl takeFirst) //download package and find amout time download
{
    qDebug() << __FUNCTION__;
    m_time.start(); // start time for find amount of time download package from link
    m_urlTakeFirstItemFedoraUpdatesRepository = takeFirst.toString(); // save value takefirts to variable m_urlTakeFirstItemFedoraRepository
    m_manager = new QNetworkAccessManager(this);
    m_request.setRawHeader("Range" , "bytes=" + QByteArray::number(0) + "-" + QByteArray::number(BYTE_DOWNLOAD_)); // set download 500 bytes from link
    m_request.setUrl(takeFirst);
    m_manager->get(m_request);
    connect(m_manager , &QNetworkAccessManager::finished , this , &MirrorFinderClass::downloadItemsFedoraUpdateRepositoryFinished);

}

void MirrorFinderClass::downloadItemsFedoraUpdateRepositoryFinished(QNetworkReply *downloadFinishedTakeFirstFedoraUpdatesRepository)
{
    qDebug() << __FUNCTION__;

    if(downloadFinishedTakeFirstFedoraUpdatesRepository->error())
    {
        qDebug () << "Error";
        qDebug () << downloadFinishedTakeFirstFedoraUpdatesRepository->errorString();
        readLinksFirstItemOfListFedoraUpdatesRepo();
    }
    else
    {
        QString finishLinksForItems = m_urlTakeFirstItemFedoraUpdatesRepository.toString();
        m_calculateTimeFedoraUpdatesRepository.insert(m_time.elapsed() ,finishLinksForItems ); // save  links and time is key and value is links

        readLinksFirstItemOfListFedoraUpdatesRepo();
    }
}
