#include <QCoreApplication>
#include  "mirrorfinderclass.h"
#include <QLoggingCategory>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QLoggingCategory::setFilterRules("*.debug=true");
    MirrorFinderClass s;
    s.startPrograme();
    return a.exec();
}
